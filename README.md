# SuperDataScience 99 Days

## Course Path

1. [Careers in Data Science](https://www.udemy.com/course/careers-in-data-science-a-ztm/)
2. [Tableau 2020: Hands-On Training for Data Science](https://www.udemy.com/course/tableau10/)
3. [Python: Python For Data Science](https://www.udemy.com/course/python-coding/)
4. [SQL & Database Design: Learn MS SQL Server + PostgreSQL](https://www.udemy.com/course/sqldatabases/)
5. [Statistics for Business Analytics and Data Science](https://www.udemy.com/course/data-statistics/)
6. [Machine Learning A-Z™: Hands-On Python & R In Data Science](https://www.udemy.com/course/machinelearning/) Up to Module 33

## Specializations:

Once the core six are completed pick a specialization

1. Machine Learning:
   - [Machine Learning A-Z™: Hands-On Python & R In Data Science](https://www.udemy.com/course/machinelearning/) Up to Module 44
2. Data Visualization:
   - [Tableau 20 Advanced Training: Master Tableau in Data Science](https://www.udemy.com/course/tableau10-advanced/)
   - [Tableau Expert: Top Visualization Techniques in Tableau 10](https://www.udemy.com/course/mastering-top-visualization-techniques-in-tableau/)
3. Data Wrangling:
   - [R Programming: Advanced Analytics In R For Data Science](https://www.udemy.com/course/r-analytics/)
   - [Data Manipulation in Python: A Pandas Crash Course](https://www.udemy.com/course/data-manipulation-in-python/)

