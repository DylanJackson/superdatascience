# Careers In Data Science

## What is a Data Science

​	Data Science is the science of leveraging data to create additional value from it

## What Does a Data Scientist Do?

Extracting insight from raw data and creating tools, visualizations, and apps that based on those insights for either business or consumer use.

## Getting Practical Experience

-  Freelance on UpWork

## Basic Requirements

- Programming knowledge
- Understanding of statistics and probabilities
- Domain you're apply the science to

# Promoting Yourself

## Preparing a CV / Portfolio

- Show Project/Job Contributions
- Impact Generated
- Added Value as a result

- Have 2 Versions, 1 page, and 3 page

## Places to Promote Skills

- LinkedIn
- Personal Blog, update once a week
- Kaggle, data science competitions
- GitLab
- Tableau Public

# Interview

- Research the Company
- Look at resource for common questions asked
- Prepare questions to ask the recruiter
  - How the company grows
  - How the company develops
  - Opportunities for Learning
  - New tools or tech they are using

## Roles in Data Science

- Business Analyst: (67k Avg Salary)
  - Translating numbers into charts
  - Data mining, AB Testing
  - BI Reports
- Data Analyst:    (73k Avg Salary)
  - Working in R and Python
  - Predictive Modeling and Forecasting
- Data Modeler:    (81k Avg Salary)
  - Expert of Modeling data and applying algorithms
- Data Scientist:    (121k Avg Salary)
  - Creating Tools from the Modeling data and algorithms
- Data Science Manager:    (134k Avg Salary)
  - Manager of a Data Science Team
  - Requires knowledge of Data Science to effectively allocate the Team

## Steps In a Data Science Project:

1. Identify the Question
2. Collect and Prepare the Data (60-80% of project time)
3. Analyze and Model the Data
4. Visualize the Insights
5. Present the findings

## Statistics to Focus On:

- Statistic Test - Identifying Statistically significant variables
  - Understanding P Values and eventually Logistical Regression and Linear Regression
- Time Models
  - Correlating the past to predict the future
  - RMI Models, Ahriman models, Time Series Analysis, Time Series Prediction
- Central Limit Theorem
- Law of Large Numbers
  - Observed values converge with expected values the larger a data set gets

## Recommended Machine Learning Algorithms

- XG Boost
- Neural Network Deep Learning
- Logistical Regression

## R vs Python

- R
  - For statistics
  - Association Rule Learning
- Python
  - For Machine Learning, A.I. and Deep Learning

## Additional Resources

- Podcasts:
  - https://www.superdatascience.com/sds-041-inspiring-journey-totally-different-background-data-science/

- Books:
  - Statistics
    - [Think Stats](https://www.amazon.com/Think-Stats-Exploratory-Data-Analysis/dp/1491907339/ref=sr_1_1?dchild=1&keywords=think+stats&qid=1622649044&sr=8-1)
    - [Bayesian Methods for Hackers](https://www.amazon.com/Bayesian-Methods-Hackers-Probabilistic-Addison-Wesley/dp/0133902838/ref=sr_1_2?dchild=1&keywords=bayesian+methods&qid=1622649109&sr=8-2)
    - [Introduction to Statistical Learning](https://www.amazon.com/Introduction-Statistical-Learning-Applications-Statistics/dp/1461471370/ref=sr_1_1?crid=2LEEQ2UG049TV&dchild=1&keywords=an+introduction+to+statistical+learning&qid=1622649217&sprefix=an+introduction+to+statistic%2Caps%2C161&sr=8-1)
    - [The Elements of Statistical Learning](https://www.amazon.com/Elements-Statistical-Learning-Prediction-Statistics/dp/0387848576/ref=sr_1_1?crid=3AKVDY56UFMI2&dchild=1&keywords=the+elements+of+statistical+learning&qid=1622649261&sprefix=the+elements+of+%2Caps%2C187&sr=8-1)
  - Machine Learning
    - [Understanding Machine Learning](https://www.amazon.com/Understanding-Machine-Learning-Theory-Algorithms/dp/1107057132/ref=sr_1_3?dchild=1&keywords=understanding+machine+learning&qid=1622649161&sr=8-3)
    - [Machine Learning Yearning](https://d2wvfoqc9gyqzf.cloudfront.net/content/uploads/2018/09/Ng-MLY01-13.pdf)
  - Data Mining
    - [A Programmers Guide to Data Mining](http://guidetodatamining.com/)
    - [Mining of Massive Datasets](https://www.amazon.com/Mining-Massive-Datasets-2-Ed/dp/1316638499/ref=sr_1_3?crid=TXQRTGD4QJZT&dchild=1&keywords=mining+of+massive+datasets&qid=1622649804&s=books&sprefix=mining+of+mass%2Cstripbooks%2C156&sr=1-3)
  - Data Science
    - [Foundations of Data Science](https://www.amazon.com/Foundations-Data-Science-Avrim-Blum/dp/1108485065)
  - Deep Learning
    - [Deep Learning](https://www.amazon.com/Deep-Learning-Adaptive-Computation-Machine/dp/0262035618/ref=sr_1_3?dchild=1&keywords=deep+learning+an+mit+press+book&qid=1622649904&s=books&sr=1-3)
- Courses:
  - Machine Learning by Andrew Ng

